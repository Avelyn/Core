package com.gitlab.avelyn.core.base;

import com.gitlab.avelyn.architecture.base.Toggleable;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.Optional;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import static com.gitlab.avelyn.core.base.Commands.*;
import static com.gitlab.avelyn.core.utilities.Fluency.cast;
import static com.gitlab.avelyn.core.utilities.Fluency.tryOr;
import static java.util.Optional.empty;
import static java.util.Optional.of;

//TODO Temporary, update to stream?
public interface Commands {
	CommandExecutor DEFAULT_EXECUTOR = (sender, $, label, args) -> false;
	
	static Toggleable handle(Object name, CommandExecutor executor) {
		return new Toggleable() {
			PluginCommand command() { return Bukkit.getServer().getPluginCommand(name.toString()); }
			
			@Override public Toggleable enable() { if (!isEnabled()) command().setExecutor(executor); return this; }
			@Override public Toggleable disable() { if (isEnabled()) command().setExecutor(DEFAULT_EXECUTOR); return this; }
			@Override public boolean isEnabled() {
			    final PluginCommand command = command();
			    return command != null && command.getExecutor() == executor;
			}
		};
	}
	static Toggleable handle(Object name, BiPredicate<CommandSender, Arguments> handler) {
		return handle(name, (sender, $, label, args) -> handler.test(sender, new Arguments() {
			int index;
			@Override public Optional<String> next() {
				return index < args.length ? of(args[index++]) : empty();
			}
		}));
	}
	
	interface Arguments {
		Optional<String> next();
		
		default <T> void next(
				Function<Arguments, Optional<T>> adapter,
				Runnable callback
		) { next(adapter, $ -> callback.run()); }
		
		default <T> void next(
				Function<Arguments, Optional<T>> adapter,
				Consumer<T> callback
		) { next(adapter).ifPresent(callback); }
		
		default <T> Optional<T> next(
				Function<Arguments, Optional<T>> adapter
		) { return adapter.apply(this); }
	}
	
	//TODO do we really want these?
	static BiPredicate<CommandSender, Arguments> fromPlayers(BiPredicate<Player, Arguments> handler) {
		return (sender, args) -> sender instanceof Player && handler.test((Player) sender, args);
	}
	static BiPredicate<CommandSender, Arguments> fromConsole(BiPredicate<ConsoleCommandSender, Arguments> handler) {
		return (sender, args) -> sender instanceof ConsoleCommandSender && handler.test((ConsoleCommandSender) sender, args);
	}
	
	interface Adapter<Type> extends Function<Arguments, Optional<Type>> {}
	
	//--Integer--
	static Adapter<Long> integer(Predicate<Long> filter) {
		return args -> args.next().map(tryOr(Long::new, null)).filter(filter);
	}
	static Adapter<Long> integer() { return integer($ -> true); }
	
	//--Real--
	static Adapter<Double> real(Predicate<Double> filter) {
		return args -> args.next().map(tryOr(Double::new, null)).filter(filter);
	}
	static Adapter<Double> real() { return real($ -> true); }
	
	//--Enumeration--
	static <Type extends Enum<Type>> Adapter<Type> enumeration(Class<Type> type, Predicate<Type> filter) {
		return args -> args.next().flatMap(arg -> Arrays.stream(type.getEnumConstants()).filter(value ->
			value.name().toLowerCase().startsWith(arg)
		).findAny()).filter(filter);
	}
	static <Type extends Enum<Type>> Adapter<Type> enumeration(Class<Type> type) { return enumeration(type, $ -> true); }
	
	//--Player--
	static Adapter<OfflinePlayer> offlinePlayer(Predicate<OfflinePlayer> filter) {
		return args -> args.next().map(Bukkit::getOfflinePlayer).filter(filter);
	}
	static Adapter<OfflinePlayer> offlinePlayer() { return offlinePlayer($ -> true); }
	static Adapter<Player> player(Predicate<Player> filter) {
		return args -> args.next().flatMap(arg -> Bukkit.getOnlinePlayers().stream().filter(player ->
				player.getName().toLowerCase().startsWith(arg)
		).findAny()).<Player>map(cast()).filter(filter);
	}
	static Adapter<Player> player() { return player($ -> true); }
}