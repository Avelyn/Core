package com.gitlab.avelyn.core.base;

import com.gitlab.avelyn.architecture.base.Toggleable;
import com.gitlab.avelyn.core.utilities.StubPlugin;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.plugin.EventExecutor;
import org.bukkit.plugin.Plugin;

import java.util.EnumMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Predicate;

import static com.gitlab.avelyn.core.base.Events.ListenerList.*;
import static net.jodah.typetools.TypeResolver.Unknown;
import static net.jodah.typetools.TypeResolver.resolveRawArgument;
import static org.bukkit.Bukkit.getPluginManager;
import static org.bukkit.event.EventPriority.NORMAL;
import static org.bukkit.event.HandlerList.unregisterAll;

@SuppressWarnings("all")
public interface Events {
	static <Type extends Event> Toggleable listen(
			Class<Type> type,
			EventPriority priority,
			Consumer<Type> listener
	) { return new Toggleable() {
		final ListenerList listeners = HANDLER_LISTS.computeIfAbsent(type, $ ->
				new EnumMap<>(EventPriority.class)
		).computeIfAbsent(priority, $ -> new ListenerList());
		
		@Override
		public Toggleable enable() {
			if (listeners.isEmpty() & listeners.add((Consumer<Event>) listener))
				getPluginManager().registerEvent(type, listeners, priority, listeners, STUB_PLUGIN);
			return this;
		}
		
		@Override
		public Toggleable disable() {
			if (listeners.remove(listener) && listeners.isEmpty()) unregisterAll(listeners);
			return this;
		}
		
		@Override
		public boolean isEnabled() { return listeners.contains(listener); }
	}; }
	
	static <Type extends Event> Toggleable listen(
			EventPriority priority,
			Consumer<Type> listener
	) {
		final Class<?> type = resolveRawArgument(Consumer.class, listener.getClass());
		if (type != Unknown.class) return listen((Class<Type>) type, priority, listener);
		throw new IllegalArgumentException(ERROR_RESOLVE);
	}
	static <Type extends Event & Cancellable> Toggleable listen(
			EventPriority priority,
			Predicate<Type> listener
	) { return listen(priority, (Type event) -> event.setCancelled(listener.test(event))); }
	
	static <Type extends Event> Toggleable listen(Consumer<Type> listener) { return listen(NORMAL, listener); }
	static <Type extends Event & Cancellable> Toggleable listen(Predicate<Type> listener) { return listen(NORMAL, listener); }
	
	//TODO rename this?
	static <Type extends Event & Cancellable> Predicate<Type> uncancelled(Predicate<Type> listener) {
		return event -> event.isCancelled() || listener.test(event);
	}
	
	class ListenerList extends HashSet<Consumer<Event>> implements Listener, EventExecutor {
		static final Plugin STUB_PLUGIN = new StubPlugin("StubPlugin Events", () -> true);
		static final String ERROR_RESOLVE = "Failed to resolve type, use 'listen(Class<Type>, EventPriority, Consumer<Type>)' instead.";
		static final Map<Class, Map<EventPriority, ListenerList>> HANDLER_LISTS = new IdentityHashMap<>();
		
		@Override
		public void execute(Listener $, Event event) { for (Consumer<Event> listener : this) { listener.accept(event); } }
	}
}