package com.gitlab.avelyn.core.components;

import com.gitlab.avelyn.architecture.base.Toggleable;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

public class ComponentPlugin extends JavaPlugin {
    private final List<Runnable> enableListeners = new ArrayList<>();
    private final List<Runnable> disableListeners = new ArrayList<>();
    
    @Override public final void onEnable() {
        getEnableListeners().forEach(Runnable::run);
    }
    @Override public final void onDisable() {
        getDisableListeners().forEach(Runnable::run);
    }
    
    public final ComponentPlugin onEnable(Runnable... listeners) {
        getEnableListeners().addAll(asList(listeners));
        return this;
    }
    public final ComponentPlugin onDisable(Runnable... listeners) {
        getDisableListeners().addAll(asList(listeners));
        return this;
    }
    
    public <Child extends Toggleable> Child child(Child child) { 
        onEnable(child::enable).onDisable(child::disable);
        return (Child) (isEnabled() ? child.enable() : child.disable());
    }
    public Toggleable[] children(Toggleable... children) { 
        for (Toggleable child : children) child(child); 
        return children;
    }
    
    public final List<Runnable> getEnableListeners() { return enableListeners; }
    public final List<Runnable> getDisableListeners() { return disableListeners; }
}