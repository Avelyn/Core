package com.gitlab.avelyn.core.utilities;

import com.gitlab.avelyn.architecture.base.Toggleable;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPhysicsEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;
import org.jetbrains.annotations.NotNull;

import java.util.function.Consumer;
import java.util.function.Predicate;

import static com.gitlab.avelyn.core.base.Events.listen;
import static org.bukkit.Effect.STEP_SOUND;
import static org.bukkit.entity.EntityType.FALLING_BLOCK;

public interface Disable {
	//--Physics--
	static Toggleable physics(Predicate<Block> blocks) {
		return listen((BlockPhysicsEvent event) -> blocks.test(event.getBlock()));
	}
	static Toggleable physics() { return physics(block -> true); }
	
	//--Explosions--
	static Toggleable explosions(Predicate<Entity> entities) {
		return listen((EntityExplodeEvent event) -> entities.test(event.getEntity()));
	}
	static Toggleable explosions() { return explosions(entity -> true); }
	
	//--Block Break--
	static Toggleable blockBreak(Predicate<Player> players, Predicate<Block> blocks) {
		return listen((BlockBreakEvent event) -> players.test(event.getPlayer()) && blocks.test(event.getBlock()));
	}
	static Toggleable blockBreak(Predicate<Player> players) { return blockBreak(players, $ -> true); }
	static Toggleable blockBreak() { return blockBreak($ -> true); }
	
	//--Block Place--
	static Toggleable blockPlace(Predicate<Player> players, Predicate<Block> blocks) {
		return listen((BlockPlaceEvent event) -> players.test(event.getPlayer()) && blocks.test(event.getBlock()));
	}
	static Toggleable blockPlace(Predicate<Player> players) { return blockBreak(players, $ -> true); }
	static Toggleable blockPlace() { return blockBreak($ -> true); }
	
	//--Item Pickup--
	static Toggleable itemPickup(Predicate<Entity> entities, Predicate<Item> items) {
		return listen((EntityPickupItemEvent event) -> entities.test(event.getEntity()) && items.test(event.getItem()));
	}
	static Toggleable itemPickup(Predicate<Entity> entities) { return itemPickup(entities, item -> true); }
	static Toggleable itemPickup() { return itemPickup(entity -> true); }
	
	//TODO got to here and then realized this should be completely redone.
	
	//Drop Item
	@NotNull
	static Toggleable dropItem(@NotNull Predicate<Player> players) {
		return dropItemItemFilter(players, item -> true);
	}
	
	@NotNull
	static Toggleable dropItem(@NotNull Predicate<Player> players, @NotNull Predicate<MaterialData> filter) {
		return dropItemItemStackFilter(players, itemStack -> filter.test(itemStack.getData()));
	}
	
	@NotNull
	static Toggleable dropItemItemStackFilter(@NotNull Predicate<Player> players, @NotNull Predicate<ItemStack> filter) {
		return dropItemItemFilter(players, item -> filter.test(item.getItemStack()));
	}
	
	@NotNull
	static Toggleable dropItemItemFilter(@NotNull Predicate<Player> players, @NotNull Predicate<Item> filter) {
		return listen((PlayerDropItemEvent event) -> {
			if (players.test(event.getPlayer()) && filter.test(event.getItemDrop())) {
				event.setCancelled(true);
			}
		});
	}
	
	//Damage
	@NotNull
	static Toggleable damage() {
		return damage(player -> true, event -> true);
	}
	
	@NotNull
	static Toggleable damage(@NotNull Predicate<Player> players) {
		return damage(players, event -> true);
	}
	
	@NotNull
	static Toggleable damage(@NotNull Predicate<Player> players, EntityDamageEvent.DamageCause cause) {
		return damage(players, event -> event.getCause().equals(cause));
	}
	
	@NotNull
	static Toggleable damage(@NotNull Predicate<Player> players, @NotNull Predicate<EntityDamageEvent> filter) {
		return listen((EntityDamageEvent event) -> {
			if (filter.test(event) &&
					event.getEntity() instanceof Player &&
					players.test((Player) event.getEntity()))
				event.setCancelled(true);
		});
	}
	
	//PvP
	@NotNull
	static Toggleable pvp() {
		return pvp(player -> true);
	}
	
	@NotNull
	static Toggleable pvp(@NotNull Predicate<Player> players) {
		return pvp(players, player -> true);
	}
	
	@NotNull
	static Toggleable pvp(@NotNull Predicate<Player> players, @NotNull Predicate<Player> attackers) {
		return damage(players, event ->
				event instanceof EntityDamageByEntityEvent &&
						((EntityDamageByEntityEvent) event).getDamager() instanceof Player &&
						attackers.test((Player) ((EntityDamageByEntityEvent) event).getDamager())
		);
	}
	
	//Fall
	@NotNull
	static Toggleable fallDamage(@NotNull Predicate<Player> players) {
		return damage(players, EntityDamageEvent.DamageCause.FALL);
	}
	
	//Hunger Change
	@NotNull
	static Toggleable hungerChange() {
		return hungerChange(player -> true);
	}
	
	@NotNull
	static Toggleable hungerChange(@NotNull Predicate<Player> players) {
		return listen((FoodLevelChangeEvent event) -> {
			if (event.getEntity() instanceof Player && players.test((Player) event.getEntity()))
				event.setCancelled(true);
		});
	}
	
	//Bow
	@NotNull
	static Toggleable bowShoot(@NotNull Predicate<Player> players) {
		return listen((EntityShootBowEvent event) -> {
			if (event.getEntity() instanceof Player && players.test((Player) event.getEntity()))
				event.setCancelled(true);
		});
	}
	
	// Falling Blocks
	@NotNull
	static Toggleable fallingBlocks(@NotNull Predicate<World> worldPredicate) {
		return listen((EntityChangeBlockEvent event) -> {
			if (!event.getEntityType().equals(FALLING_BLOCK))
				return;
			if (!worldPredicate.test(event.getBlock().getWorld()))
				return;
			if (event.getEntity() instanceof FallingBlock) {
				FallingBlock block = (FallingBlock) event.getEntity();
				block.getWorld().playEffect(block.getLocation(), STEP_SOUND, block.getMaterial());
			}
			event.getEntity().remove();
			event.setCancelled(true);
		});
	}
	
	//GameRule
	static Consumer<World> gameRule(String gameRule, String value) {
		return world -> world.setGameRuleValue(gameRule, value);
	}
	
	static Consumer<World> gameRule(String gameRule) {
		return gameRule(gameRule, "false");
	}
	
	static Consumer<World> time() {
		return gameRule("doDaylightCycle");
	}
	
	static Consumer<World> time(long time) {
		Consumer<World> stopTime = time();
		return world -> {
			world.setTime(time);
			stopTime.accept(world);
		};
	}
	
	static Consumer<World> currentStorm() {
		return world -> world.setStorm(false);
	}
	
	static Consumer<World> entityDrops() {
		return gameRule("doEntityDrops");
	}
	
	static Consumer<World> fireSpread() {
		return gameRule("doFireTick");
	}
	
	static Consumer<World> mobLoot() {
		return gameRule("doMobLoot");
	}
	
	static Consumer<World> mobSpawning() {
		return gameRule("doMobSpawning");
	}
	
	static Consumer<World> mobGriefing() {
		return gameRule("mobGriefing");
	}
	
	static Consumer<World> naturalRegeneration() {
		return gameRule("naturalRegeneration");
	}
	
	static Consumer<World> randomTickSpeed() {
		return gameRule("randomTickSpeed", "0");
	}
	
	static Consumer<World> deathMessages() {
		return gameRule("showDeathMessages");
	}
}
