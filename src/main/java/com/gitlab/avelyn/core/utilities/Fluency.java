package com.gitlab.avelyn.core.utilities;

import javafx.util.Pair;

import java.util.function.*;
import java.util.regex.Pattern;

import static java.util.function.Function.identity;

public interface Fluency {
	//--Conversion--
	static <Type> Predicate<Type> asString(Predicate<String> condition) { return it -> condition.test(it.toString()); }
	static <Type> Predicate<Type> asNumber(Predicate<Number> condition) { return it -> {
			try { return condition.test(new Double(it.toString())); }
			catch (NumberFormatException $) { return false; }
	}; }
	static <Type> Predicate<Type> asInt(IntPredicate condition) { return asNumber(it -> condition.test(it.intValue())); }
	static <Type> Predicate<Type> asLong(LongPredicate condition) { return asNumber(it -> condition.test(it.longValue())); }
	static <Type> Predicate<Type> asDouble(DoublePredicate condition) { return asNumber(it -> condition.test(it.doubleValue()));  }
	static <Type extends Enum<Type>> Predicate<Type> enumName(Predicate<String> condition) { return it -> condition.test(it.name()); }
	
	//--Operators--
	static <Type> Predicate<Type> not(Predicate<Type> predicate) { return predicate.negate(); }
	static <Type> Predicate<Type> and(Predicate<Type> first, Predicate<Type> second) { return first.and(second); }
	static <Type> Predicate<Type> or(Predicate<Type> first, Predicate<Type> second) { return first.or(second); }
	static <Type> Predicate<Type> andNot(Predicate<Type> first, Predicate<Type> second) { return first.and(second.negate()); }
	static <Type> Predicate<Type> orNot(Predicate<Type> first, Predicate<Type> second) { return first.and(second.negate()); }
	static <Type> Predicate<Type> both(Predicate<Type> first, Predicate<Type> second) { return and(first, second); }
	static <Type> Predicate<Type> either(Predicate<Type> first, Predicate<Type> second) { return or(first, second); }
	
	//--Comparison--
	static <Type extends Comparable<Type>> Predicate<Type> between(Type min, Type max) {
		return it -> it.compareTo(min) > 0 && it.compareTo(max) < 0;
	}
	static <Type extends Comparable<Type>> Predicate<Type> greaterThan(Type min) { return it -> it.compareTo(min) > 0; }
	static <Type extends Comparable<Type>> Predicate<Type> greaterThanOrEqualTo(Type min) { return it -> it.compareTo(min) >= 0; }
	static <Type extends Comparable<Type>> Predicate<Type> lessThan(Type max) { return it -> it.compareTo(max) < 0; }
	static <Type extends Comparable<Type>> Predicate<Type> lessThanOrEqualTo(Type max) { return it -> it.compareTo(max) <= 0; }
	static <Type extends Comparable<Type>> Predicate<Type> equalTo(Type value) { return it -> it.compareTo(value) == 0; }
	
	static IntPredicate between(int min, int max) { return it -> it > min && it < max; }
	static IntPredicate greaterThan(int min) { return it -> it > min; }
	static IntPredicate greaterThanOrEqualTo(int min) { return it -> it >= min; }
	static IntPredicate lessThan(int value) { return it -> it < value; }
	static IntPredicate lessThanOrEqualTo(int value) { return it -> it <= value; }
	static IntPredicate equalTo(int value) { return it -> it == value; }
	
	static LongPredicate greaterThan(long value) { return it -> it > value; }
	static LongPredicate greaterThanOrEqualTo(long value) { return it -> it >= value; }
	static LongPredicate lessThan(long value) { return it -> it < value; }
	static LongPredicate lessThanOrEqualTo(long value) { return it -> it <= value; }
	static LongPredicate equalTo(long value) { return it -> it == value; }
	
	static DoublePredicate greaterThan(double value) { return it -> it > value; }
	static DoublePredicate greaterThanOrEqualTo(double value) { return it -> it >= value; }
	static DoublePredicate lessThan(double value) { return it -> it < value; }
	static DoublePredicate lessThanOrEqualTo(double value) { return it -> it <= value; }
	static DoublePredicate equalTo(double value) { return it -> it == value; }
	
	//--String--
	static <Type> Predicate<Type> matching(Object pattern) { return matching(Pattern.compile(pattern.toString())); }
	static <Type> Predicate<Type> matching(Pattern pattern) { return it -> pattern.matcher(it.toString()).find(); }
	static <Type> Predicate<Type> startingWith(Object sequence) { return it -> it.toString().startsWith(sequence.toString()); }
	static <Type> Predicate<Type> endingWith(Object sequence) { return it -> it.toString().endsWith(sequence.toString()); }
	static <Type> Predicate<Type> contains(Object sequence) { return it -> it.toString().contains(sequence.toString()); }
	static <Type> Predicate<Type> length(IntPredicate condition) { return it -> condition.test(it.toString().length()); }

	
	//--Pair--
	static <From, To> Function<From, Pair<From, To>> toPair(To value) { return toPair($ -> value); }
	static <From, To> Function<From, Pair<From, To>> toPair(Supplier<To> valueSupplier) { return toPair($ -> valueSupplier.get()); }
	static <From, To> Function<From, Pair<From, To>> toPair(Function<From, To> valueMap) { return toPair(identity(), valueMap); }
	static <From, Key, Value> Function<From, Pair<Key, Value>> toPair(Supplier<Key> keySupplier, Function<From, Value> valueMap) {
		return toPair($ -> keySupplier.get(), valueMap);
	}
	static <From, Key, Value> Function<From, Pair<Key, Value>> toPair(Key key, Function<From, Value> valueMap) {
		return toPair($ -> key, valueMap);
	}
	static <From, Key, Value> Function<From, Pair<Key, Value>> toPair(Function<From, Key> keyMap, Function<From, Value> valueMap) {
		return from -> new Pair<>(keyMap.apply(from), valueMap.apply(from));
	}


	//--Constants--
	static <Type> Predicate<Type> just(boolean value) { return $ -> value; }
	static <From, To> Function<From, To> just(To value) { return $ -> value; }
	@SuppressWarnings("unchecked") static <From, To> Function<From, To> cast() { return value -> (To) value; }
	
	static <From, To> Function<From, To> tryOrGet(Function<From, To> function, Supplier<To> or) { return from -> {
		try { return function.apply(from); }
		catch (Throwable $) { return or.get(); }
	}; }
	static <From, To> Function<From, To> tryOr(Function<From, To> function, To or) { return tryOrGet(function, () -> or); }
	
	static RuntimeException runtime(Throwable throwable) {
		return throwable instanceof RuntimeException ? (RuntimeException) throwable : new RuntimeException(throwable);
	}
	
}